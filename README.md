# Moloblocks

![](logo.png)

Hugo theme to integrate Blocks partials into any other theme. Blocks made with Bulma CSS.

## Instructions

To use these blocks in a Hugo site, you must

- Add this theme to the themes list of your website. In the `config.toml` add

```toml
theme = [<your-current-theme>, "moloblocks"]
```

- Add the 'blocks' partial calling inside the corresponding layout or page. If you want to display blocks on every page you can modify `layouts/_default/single.html`

```html
{{ define "main" }}
  {{ partial "blocks" . }}
{{ end }}
```

- Create markdown content with the corresponding frontmatter structure according to the block you are using. An exmaple `content/index.md` file could look like this

```markdown
+++
date = "2018-06-24T18:52:31-03:00"
title = "Website Title"
type = "page"
[[blocks]]
  background_image = "uploads/2018/bg.jpg"
  navbar = "false"
  template = "hero"
  title = "Hero Title"
[[blocks]]
  template = "section"
  title = "Section title"
+++
```

- Include blocks custom CSS style in your HTML

```html
<!-- include single CSS file -->
{{ $blocksStyle := resources.Get "sass/blocks/style.scss" | resources.ToCSS | resources.Minify }}
<link rel="stylesheet" href="{{ $blocksStyle.Permalink }}">

<!-- ... or include into a bundle file -->
{{ $style := resources.Get "sass/style.scss" | resources.ToCSS }}
{{ $blocksStyle := resources.Get "sass/blocks/style.scss" | resources.ToCSS }}
{{ $siteStyles := (slice $style $blocksStyle) | resources.Concat "css/bundle.css" | resources.Minify }}
<link rel="stylesheet" href="{{ $siteStyles.Permalink }}">
```

## Navbar Usage

The navbar will automatically render a menu named `main`. You can define this menu as a [regular Hugo Menu](https://gohugo.io/content-management/menus/).

The navbar block has some special usage cases that must be noted. It can be used in three different ways:

### Usage Case 1: As a regular block defined in the FrontMatter

```markdown
+++
title = "Page Title"
type = "page"
[[blocks]]
  template = "navbar"
  class = "is-primary"
[[blocks]]
  # ... more blocks definitions ...
+++
```

### Usage Case 2: As a block inside other block

Currently only the `hero` block accepts a navbar inside.

```markdown
[[blocks]]
  background_image = "bg-hero.jpg"
  template = "hero"
  class = "is-fullheight"
  title = "Hero Title"
  [blocks.navbar]
    class = "is-primary"
[[blocks]]
  # ... more blocks definitions ...
```

### Usage Case 3: As a template layout outside the FrontMatter

In this case we insert the block directly on our template files, as a partial. We must pass some values to the navbar to work correctly.

```html
<!-- creates config options to pass to navbar block -->
{{ $navbar_config := (dict "class" "is-primary") }}
{{ $context := (dict "Site" .Site "Block" $navbar_config) }}
{{ partial "blocks/navbar.html" $context }}
```

To avoid the problem of having 2 navbars rendered (one included in a general layout, maybe in `_baseof.html` and another one defined in a page's frontmatter), you could add a condition to the general inclusion part, and use this condition in specific page's frontmatter. We use this trick on Molotec's website.

#### Example of `hide_navbar` condition flag

On the general template file, add the navbar conditionally, checking for `.Params.hide_navbar` value.

```html
{{ if not .Params.hide_navbar }}
  <!-- creates config options to pass to navbar block -->
  {{ $navbar_config := (dict "class" "is-primary is-fixed-top") }}
  {{ $context := (dict "Site" .Site "Block" $navbar_config) }}
  {{ partial "blocks/navbar.html" $context }}
{{ end }}
```

An then, on every page that defines it's own navbar in it's frontmatter, you sohuld set this flag

```markdown
+++
title = "Some Page Title"
hide_navbar = true
[blocks.navbar]
  template = "navbar"
  "class" = "is-primary"
```

## Available Blocks

### contact

- `id` (string)
- `class` (string)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- `action` (string)

### contact-simple

- `id` (string)
- `class` (string)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- `action` (string)

### features

- `id` (string)
- `class` (string)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- [`features`]
  - `title` (string)
  - `image` (path)
  - `description` (string)

### hero

- `id` (string)
- `class` (string)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- `background_image` (path)
- `image` (path)
- `navbar` (bool)

### logos

- `id` (string)
- `class` (string)
- `color` (string)(CSS color value)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- [`logos`]
  - `title` (string)
  - `image` (path)
  - `description` (string)
  - `url` (string)

### members

- `id` (string)
- `class` (string)
- `title` (string)
- [`members`]
  - `name` (string)
  - `photo` (path)
  - `quote` (string)
  - [`links`]
    - `title` (string)
    - `url` (string)

### navbar

- `id` (string)
- `class` (string)
- `logo` (path)

### section

- `id` (string)
- `class` (string)
- `color` (string)(CSS color value)
- `title` (string)
- `subtitle` (string)
- `description` (markdown)
- `cta`
  - `class` (string)
  - `title` (string)
  - `url` (string)
- `aos`
  - `animation`
    - `effect` (string)
    - `duration` (string)
    - `once` (string)

### services

- `id` (string)
- `class` (string)
- `title` (string)
- `subtitle` (string)
- [`services`]
  - `inverted` (bool)
  - `title` (string)
  - `image` (path)
  - `description` (string)
  - `cta`
    - `title` (string)
    - `url` (string)
  - [`points`]

### values

- `id` (string)
- `class` (string)
- `title` (string)
- [`values`]
  - `title` (string)
  - `icon` (string)
  - `description` (string)

### youtube-embed

- `id` (string)
- `class` (string)
- `videoID` (string) (Ex: https://www.youtube.com/watch?v=dQw4w9WgXcQ -> videoID = dQw4w9WgXcQ)

## Changelog

See [CHANGELOG.md](CHANGELOG.md) file.
